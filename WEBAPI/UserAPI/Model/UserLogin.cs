﻿namespace UserAPI.Model
{
    public class UserLogin
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
